function populateHtml(){
  alert("There are " + recipes.length + " recipes!");
  var div = document.getElementById("recipesDiv");
  for(var iterator = 0; iterator < recipes.length; iterator++){
    var image = document.createElement("img");
    image.setAttribute("id",  iterator);
    image.setAttribute("src", recipes[iterator].imageLink);
    image.setAttribute("height", "200");
    image.setAttribute("width", "300");
    image.setAttribute("onClick", "onImageClick(" + iterator + ")");
    div.appendChild(image);
  }
}

function onImageClick(id){
  var recipeImage = "<div><center><img align=middle src=" + recipes[id].imageLink + "width=400 height=300></img></center></div>";
  var recipeDescription = "<div><h3>Description</h3><p>" + recipes[id].description + "</p></div>";
  var recipeIngredients = "<div><h3>Ingredients</h3><p>" + recipes[id].ingredients + "</p></div>";
  var win=window.open("","","width=700,height=600");
  win.document.open();
  win.document.write(recipeImage);
  win.document.write(recipeDescription);
  win.document.write(recipeIngredients);
  win.document.close();
}

populateHtml();
