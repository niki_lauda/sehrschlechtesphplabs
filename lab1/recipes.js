var recipes = [
  {
    imageLink: "https://d3hvwccx09j84u.cloudfront.net/web/image/crispy-sunchoke-and-quinoa-salad-bfe2068c.jpg?t=20160929000923",
    description: "Despite their name, sunchokes, aka Jerusalem artichokes, have nothing at all to do with Jerusalem and are not, in fact, artichokes. Rather, this nutty and slightly sweet tuber is a type of sunflower and received its name from the Italian word for the plant, “girasole.” When thinly sliced and roasted, sunchokes become irresistibly crispy and pair perfectly with hearty kale and quinoa.",
    ingredients: "Sunchokes, Tuscan Kale, Mint, Scallions, Quinoa, Dried Cranberries, White Wine Vinegar, Feta Cheese (Milk), Olive Oil*, Salt*, Pepper*"
  },
  {
    imageLink: "https://d3hvwccx09j84u.cloudfront.net/web/image/sweet-italian-sausages-with-freg-2ecf4339.jpg?t=20160929011018",
    description: "If you're a fan of Israeli couscous, then you'll love this dish. This pearled pasta is tossed with mushrooms, topped with spinach, and held together with creamy Parmesan cheese. Needless to say, this dish has comfort food written all over it.",
    ingredients:"Lean Sweet Italian Sausage Meat (Pork), Israeli Couscous (Wheat), Onion, Garlic, Button Mushrooms, Spinach, Dried Oregano, Parmesan Cheese (Milk), Chicken Stock Concentrate, Olive Oil*, Salt*, Pepper*"
  },
  {
    imageLink: "https://d3hvwccx09j84u.cloudfront.net/web/image/uk-pork-and-apple-burgers-w41-ca248230.jpg?t=20160928220650",
    description: "Nothing screams fall more than apples, and nothing pairs better with apples than pork (except maybe cinnamon sugar, but we'll stick to savory dishes for now). For this recipe, our chefs chose the Granny Smith variety for its hard, crisp, and slightly tart flesh. When it's shredded and mixed with pork, the result is deliciously juicy patties.",
    ingredients:"Rosemary, Yukon Potatoes, Granny Smith Apple, Ground Pork, Bun (Wheat, Milk, Eggs, Soy), Balsamic Vinegar, Spring Mix, Mayonnaise (Eggs, Soy), Olive Oil*, Salt*, Pepper*"
  },
  {
    imageLink: "https://d3hvwccx09j84u.cloudfront.net/web/image/sesame-shrimp-with-jasmine-rice-4193a1f1.jpg?t=20160928224519",
    description: "Although we love rice in all its versatile, humble, and hearty goodness, we gotta admit: sometimes, it gets a bit bland. The solution? Aromatics. Ginger and scallion, to be exact. It's one of our secrets to a super flavorful stir-fry in around 30 minutes.",
    ingredients:"Shrimp (Shellfish), Jasmine Rice, Ginger, Scallions, Garlic, Hoisin Sauce Jar (Soy), Sesame Seeds, Lime, Green Beans, Olive Oil*, Salt*, Pepper*"
  },
  {
    imageLink: "https://d3hvwccx09j84u.cloudfront.net/web/image/lasagna-baked-penne-with-kale-85133c8d.jpg?t=20160928224548",
    description: "We've taken our favorite lasagna recipe and deconstructed it for a simple dinner that's just as comforting as the classic. Although our chefs used penne in the past, we made a last-minute upgrade to fusilli. The mozzarella, ground beef, kale, and panko fit snugly inside the pasta's tight spirals for a burst of flavor in every creamy bite.",
    ingredients:"Ground Beef, Fusilli (Wheat), Onion, Garlic, Kale, Dried Oregano, Basil, Panko (Wheat, Soy), Fresh Mozzarella (Milk), Chili Flakes, Crushed Tomatoes, Olive Oil*, Salt*, Pepper*"
  },
  {
    imageLink: "https://d3hvwccx09j84u.cloudfront.net/web/image/mushroom-ravioli-gratin-c1209ef7.jpg?t=20160929014722",
    description: "Gratin is a fancy French way of saying this dish is topped with cheesy breadcrumbs and heated under the broiler until brown and crispy. So it essentially translates to “delicious,” right? But what takes this dinner to the next level is the fact that it's made in only one pot. Talk about an easy clean-up.",
    ingredients:"Mushroom Ravioli (Wheat, Milk, Eggs), Button Mushrooms, Sour Cream (Milk), Vegetable Stock Concentrate, Parmesan Cheese (Milk), Panko (Wheat, Soy), Onion, Thyme, Olive Oil*, Salt*, Pepper*"
  },
  {
    imageLink: "https://d3hvwccx09j84u.cloudfront.net/web/image/crispy-sunchoke-and-quinoa-salad-bfe2068c.jpg?t=20160929000923",
    description: "Despite their name, sunchokes, aka Jerusalem artichokes, have nothing at all to do with Jerusalem and are not, in fact, artichokes. Rather, this nutty and slightly sweet tuber is a type of sunflower and received its name from the Italian word for the plant, “girasole.” When thinly sliced and roasted, sunchokes become irresistibly crispy and pair perfectly with hearty kale and quinoa.",
    ingredients: "Sunchokes, Tuscan Kale, Mint, Scallions, Quinoa, Dried Cranberries, White Wine Vinegar, Feta Cheese (Milk), Olive Oil*, Salt*, Pepper*"
  },
  {
    imageLink: "https://d3hvwccx09j84u.cloudfront.net/web/image/sweet-italian-sausages-with-freg-2ecf4339.jpg?t=20160929011018",
    description: "If you're a fan of Israeli couscous, then you'll love this dish. This pearled pasta is tossed with mushrooms, topped with spinach, and held together with creamy Parmesan cheese. Needless to say, this dish has comfort food written all over it.",
    ingredients:"Lean Sweet Italian Sausage Meat (Pork), Israeli Couscous (Wheat), Onion, Garlic, Button Mushrooms, Spinach, Dried Oregano, Parmesan Cheese (Milk), Chicken Stock Concentrate, Olive Oil*, Salt*, Pepper*"
  },
  {
    imageLink: "https://d3hvwccx09j84u.cloudfront.net/web/image/uk-pork-and-apple-burgers-w41-ca248230.jpg?t=20160928220650",
    description: "Nothing screams fall more than apples, and nothing pairs better with apples than pork (except maybe cinnamon sugar, but we'll stick to savory dishes for now). For this recipe, our chefs chose the Granny Smith variety for its hard, crisp, and slightly tart flesh. When it's shredded and mixed with pork, the result is deliciously juicy patties.",
    ingredients:"Rosemary, Yukon Potatoes, Granny Smith Apple, Ground Pork, Bun (Wheat, Milk, Eggs, Soy), Balsamic Vinegar, Spring Mix, Mayonnaise (Eggs, Soy), Olive Oil*, Salt*, Pepper*"
  },
  {
    imageLink: "https://d3hvwccx09j84u.cloudfront.net/web/image/sesame-shrimp-with-jasmine-rice-4193a1f1.jpg?t=20160928224519",
    description: "Although we love rice in all its versatile, humble, and hearty goodness, we gotta admit: sometimes, it gets a bit bland. The solution? Aromatics. Ginger and scallion, to be exact. It's one of our secrets to a super flavorful stir-fry in around 30 minutes.",
    ingredients:"Shrimp (Shellfish), Jasmine Rice, Ginger, Scallions, Garlic, Hoisin Sauce Jar (Soy), Sesame Seeds, Lime, Green Beans, Olive Oil*, Salt*, Pepper*"
  },
  {
    imageLink: "https://d3hvwccx09j84u.cloudfront.net/web/image/lasagna-baked-penne-with-kale-85133c8d.jpg?t=20160928224548",
    description: "We've taken our favorite lasagna recipe and deconstructed it for a simple dinner that's just as comforting as the classic. Although our chefs used penne in the past, we made a last-minute upgrade to fusilli. The mozzarella, ground beef, kale, and panko fit snugly inside the pasta's tight spirals for a burst of flavor in every creamy bite.",
    ingredients:"Ground Beef, Fusilli (Wheat), Onion, Garlic, Kale, Dried Oregano, Basil, Panko (Wheat, Soy), Fresh Mozzarella (Milk), Chili Flakes, Crushed Tomatoes, Olive Oil*, Salt*, Pepper*"
  },
  {
    imageLink: "https://d3hvwccx09j84u.cloudfront.net/web/image/mushroom-ravioli-gratin-c1209ef7.jpg?t=20160929014722",
    description: "Gratin is a fancy French way of saying this dish is topped with cheesy breadcrumbs and heated under the broiler until brown and crispy. So it essentially translates to “delicious,” right? But what takes this dinner to the next level is the fact that it's made in only one pot. Talk about an easy clean-up.",
    ingredients:"Mushroom Ravioli (Wheat, Milk, Eggs), Button Mushrooms, Sour Cream (Milk), Vegetable Stock Concentrate, Parmesan Cheese (Milk), Panko (Wheat, Soy), Onion, Thyme, Olive Oil*, Salt*, Pepper*"
  }
];
