<?php
//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
//Patient Info
if($_POST['showPatients']){
  echo "<strong>Patients Info</strong></br>";
  $conn = connectToMySQL("localhost", "yevhenii", "yevhenii", "lab2");
  if(!$conn){
      echo "Connection failed</br>";
      die();
  }
  echo "Connected successfully</br>";

  $sql = "SELECT patient.pname, room.rnum, department.dtype,  doctor.docname
    FROM room
    INNER JOIN department
    ON room.depid = department.did
    INNER JOIN doctor
    ON doctor.depid = department.did
    INNER JOIN registry
    ON registry.roomid = room.rid
    INNER JOIN patient
    ON registry.patid = patient.pid;";

  $result = $conn->query($sql);

  echo '<link rel="stylesheet" type="text/css" href="styles.css">';
  if ($result->num_rows > 0) {
    echo "<table><tr><th>Patient</th><th>Room</th><th>Department Name</th><th>Doctor</th></tr><tbody>";
      while($row = $result->fetch_assoc()) {
          echo "<tr><td>" . $row["pname"]. "</td><td>" . $row["rnum"] . "</td><td>" . $row["dtype"] . "</td><td>" . $row["docname"] . "</td></tr>";
      }
    echo "</tbody></table>";
  } else {
      echo "0 results";
  }

  //Close connection
  $conn->close();
  echo "Connection closed</br>";
}

//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
//Department Info

if($_POST['showDepartments']){
  $depID = $_POST['departmentID'];
  echo "<strong>Department [" . $depID . "] Info</strong></br>";
  $conn = connectToMySQL("localhost", "yevhenii", "yevhenii", "lab2");
  if(!$conn){
      echo "Connection failed</br>";
      die();
  }
  echo "Connected successfully</br>";

  $sql = "SELECT room.rid, room.capacity, room.rnum
    FROM room
    INNER JOIN department
    ON room.depid = department.did
    WHERE room.depid = " . $depID . ";";

  $result = $conn->query($sql);

  echo '<link rel="stylesheet" type="text/css" href="styles.css">';
  if ($result->num_rows > 0) {
    echo "<table><tr><th>Room ID</th><th>Room Number</th><th>Room Capacity</th></tr><tbody>";
      while($row = $result->fetch_assoc()) {
          echo "<tr><td>" . $row["rid"]. "</td><td>" . $row["rnum"] . "</td><td>" . $row["capacity"] . "</td></tr>";
      }
    echo "</tbody></table>";
  } else {
      echo "0 results";
  }

  //Close connection
  $conn->close();
  echo "Connection closed</br>";
}

//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
//Duties Info
if($_POST['showDuties']){
  echo "<strong>Duties Info</strong></br>";
  $conn = connectToMySQL("localhost", "yevhenii", "yevhenii", "lab2");
  if(!$conn){
      echo "Connection failed</br>";
      die();
  }
  echo "Connected successfully</br>";

  $sql = "SELECT duty.day, doctor.docname, department.dtype
FROM duty
INNER JOIN doctor
ON duty.docid = doctor.docid
INNER JOIN department
ON duty.depid = department.did;";

  $result = $conn->query($sql);

  echo '<link rel="stylesheet" type="text/css" href="styles.css">';
  if ($result->num_rows > 0) {
    echo "<table><tr><th>Day</th><th>Doctor</th><th>Department</th></tr><tbody>";
      while($row = $result->fetch_assoc()) {
          echo "<tr><td>" . $row["day"]. "</td><td>" . $row["docname"] . "</td><td>" . $row["dtype"] . "</td></tr>";
      }
    echo "</tbody></table>";
  } else {
      echo "0 results";
  }

  //Close connection
  $conn->close();
  echo "Connection closed</br>";
}

//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
//Duty on Day Info
if($_POST['showDutyOn']){
  $day = $_POST['day'];
  echo "<strong>Duty on Day</strong></br>";
  $conn = connectToMySQL("localhost", "yevhenii", "yevhenii", "lab2");
  if(!$conn){
      echo "Connection failed</br>";
      die();
  }
  echo "Connected successfully</br>";

  $sql = "SELECT doctor.docname, department.dtype
FROM duty
INNER JOIN doctor
ON duty.docid = doctor.docid
INNER JOIN department
ON duty.depid = department.did
WHERE day = '".$day."';";

  $result = $conn->query($sql);

  echo '<link rel="stylesheet" type="text/css" href="styles.css">';
  if ($result->num_rows > 0) {
    echo "Duties on [<strong>" . $day ."</strong>]";
    echo "<table><tr><th>Doctor</th><th>Department</th></tr><tbody>";
      while($row = $result->fetch_assoc()) {
          echo "<tr><td>" . $row["docname"] . "</td><td>" . $row["dtype"] . "</td></tr>";
      }
    echo "</tbody></table>";
  } else {
      echo "0 results";
  }

  //Close connection
  $conn->close();
  echo "Connection closed</br>";
}

//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
//Delete Update Add Operations
if($_POST['registry']){
  echo "<strong>Registry Update Delete Add Operations</strong></br>";
  $conn = connectToMySQL("localhost", "yevhenii", "yevhenii", "lab2");
  if(!$conn){
      echo "Connection failed</br>";
      die();
  }
  echo "Connected successfully</br>";

  $sql = "SELECT * FROM registry";

  $result = $conn->query($sql);

  echo '<link rel="stylesheet" type="text/css" href="styles.css">';
  if ($result->num_rows > 0) {
    echo "Duties on [<strong>" . $day ."</strong>]";
    echo "<table><tr><th>ID</th><th>Patient ID</th><th>Doctor ID</th><th>Room ID</th></tr><tbody>";
      while($row = $result->fetch_assoc()) {
          echo "<tr><td>" . $row["regid"] . "</td><td>" . $row["patid"] . "</td><td>" . $row["docid"] . "</td><td>" . $row["roomid"] . "</td></tr>";
      }
    echo "</tbody></table>";
  } else {
      echo "0 results";
  }

  echo "<div><form action='HomeController.php' method='POST'>
        <label>ID:</label>
        <input name='id' type='text' placeholder='Enter id'/>
        <label>Patient ID:</label>
        <input name='pid' type='text' placeholder='Enter id'/>
        <label>Doctor ID:</label>
        <input name='docid' type='text' placeholder='Enter id'/>
        <label>Room ID:</label>
        <input name='rid' type='text' placeholder='Enter id'/>
        <input name='operation' type='submit'/>
  </form></div>";

  //Close connection
  $conn->close();
  echo "Connection closed</br>";
}

if($_POST['operation']){
  echo "<strong>Operation on Database done! </strong></br>";
  $conn = connectToMySQL("localhost", "yevhenii", "yevhenii", "lab2");
  if(!$conn){
      echo "Connection failed</br>";
      die();
  }
  echo "Connected successfully</br>";
  $sql = "";
  if($_POST['id'] == "" && $_POST['pid'] == "" && $_POST['docid'] == "" && $_POST['rid'] == ""){
    echo "The fields are empty, no operation can be done!";
    die();
  }
  if($_POST['id'] == ""){
    $sql = "INSERT INTO registry(patid, docid, roomid)
    VALUES('" . $_POST['pid'] . "', '" . $_POST['docid'] . "', '" . $_POST['rid'] . "');";
    echo $sql;
  }
  else if($_POST['pid'] == "" || $_POST['docid'] == "" || $_POST['rid'] == ""){
    $sql = "DELETE FROM registry WHERE regid='" . $_POST['id'] . "';";
    echo $sql;
  }
  else {
    $sql = "UPDATE registry SET patid='".$_POST['pid'] ."', docid='".$_POST['docid']."', roomid='".$_POST['rid']."' WHERE regid='".$_POST['id'] . "';";
    echo $sql;
  }

  $result = $conn->query($sql);
  echo '<link rel="stylesheet" type="text/css" href="styles.css">';
  if ($result) {
    echo "The query has been successfully performed!!!";
  } else {
      echo "ERROR! 0 results";
  }

  //Close connection
  $conn->close();
  echo "Connection closed</br>";
}


function connectToMySQL($servername, $username, $password, $database){
  $conn = new mysqli($servername, $username, $password, $database);
  if ($conn->connect_error) {
      return null;
  }
  return $conn;
}
?>
