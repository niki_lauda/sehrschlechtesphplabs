 <!DOCTYPE html>
<html>
<head>
<title>Lab2</title>
<link rel="stylesheet" type="text/css" href="styles.css"/>
</head>
<body>
  <h1>Lab2: Connecting to MySQL DB using PHP</h1>

<?php
$servername = "localhost";
$username = "yevhenii";
$password = "yevhenii";
$database = "lab2";
// Create connection
$conn = new mysqli($servername, $username, $password, $database);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}
echo "Connected successfully</br>";

$sql = "SELECT * FROM department";
$result = $conn->query($sql);

echo '<link rel="stylesheet" type="text/css" href="styles.css">';
if ($result->num_rows > 0) {
  echo "<table><tr><th>Department ID</th><th>Department Name</th><tbody>";
    while($row = $result->fetch_assoc()) {
        echo "<tr><td>" . $row["did"]. "</td><td>" .  $row["dtype"] . "</td></tr>";
    }
  echo "</tbody></table>";
} else {
    echo "0 results";
}

//Close connection
$conn->close();
echo "Connection closed</br>";
?>

<form action="HomeController.php" method="POST">
  <div>
  <input name="showDepartments" type="submit" value="Departments(Rooms, Doctors, Patients)" />
  <input name="departmentID" type="text" placeholder="Enter id of the Department"/>
  </div>
  <input name="showPatients" type="submit" value="Patients(Department, Doctor)" />
  <input name="showDuties" type="submit" value="Duties(Day, Department, Doctor)" />
  <div>
  <input name="showDutyOn" type="submit" value="Duty On Day:" />
  <input list="days" name="day" placeholder="Enter the day">
  <datalist id="days">
  <option value="Sunday">
  <option value="Monday">
  <option value="Tuesday">
  <option value="Wednesday">
  <option value="Thursday">
  <option value="Friday">
  <option value="Saturday">
  </datalist>
</div>
<input name="registry" type="submit" value="Registry Update Delete Add Operations"/>
</form>


</body>
</html>
